#!/usr/bin/env bash

echo "-------------------------------------------------"
echo "       Setup Language to US and set locale       "
echo "-------------------------------------------------"
ln -sf /usr/share/zoneinfo/America/Montevideo /etc/localtime
hwclock --systohc
sed -i 's/^#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
timedatectl --no-ask-password set-timezone America/Montevideo
timedatectl --no-ask-password set-ntp 1

touch /etc/locale.conf
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# Set keymaps
localectl --no-ask-password set-keymap us

if ! source install.conf; then
  read -p "Name your machine:" ANHOST
  read -p "Please enter username:" ANUSER
  echo "hostname=$ANHOST" >> ${HOME}/ArchNova/install.conf
  echo "username=$ANUSER" >> ${HOME}/ArchNova/install.conf
fi

touch /etc/hostname
echo "${ANHOST}" >> /etc/hostname
echo "127.0.0.1     localhost" >> /etc/hosts
echo "::1           localhost" >> /etc/hosts
echo "127.0.1.1     ${ANHOST}.localdomain  ${ANHOST}" >> /etc/hosts

if [ $(whoami) = "root"  ];
then
  useradd -m -G wheel,audio,video,optical,storage -s /bin/bash $ANUSER
  echo "Please enter your password."
  passwd $ANUSER
  echo "Now enter the root password."
  passwd
  cp -R /root/ArchNova /home/$ANUSER/
  chown -R $ANUSER: /home/$ANUSER/ArchNova
  sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
else
  echo "You are already have a user."
fi
