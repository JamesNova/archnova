#!/usr/bin/env bash

#-------------------------------------------------------------------------
#   █████╗ ██████╗  ██████╗██╗  ██╗████╗  ██╗ ██████╗ ██╗   ██╗ █████╗ 
#  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔██╗ ██║██╔═══██╗██║   ██║██╔══██╗
#  ███████║██████╔╝██║     ███████║██║ ██╗██║██║   ██║██║   ██║███████║
#  ██╔══██║██╔══██╗██║     ██╔══██║██║  ████║██║   ██║╚██╗ ██╔╝██╔══██║
#  ██║  ██║██║  ██║╚██████╗██║  ██║██║    ██║╚██████╔╝ ╚████╔╝ ██║  ██║
#  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝    ╚═╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝
#-------------------------------------------------------------------------

echo "-------------------------------------------------"
echo "------------- Setting up everything -------------"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib terminus-font
setfont ter-v22b
sed -i 's/^#Para/Para/' /etc/pacman.conf
echo -e "-------------------------------------------------------------------------"
echo -e "   █████╗ ██████╗  ██████╗██╗  ██╗████╗  ██╗ ██████╗ ██╗   ██╗ █████╗    "
echo -e "  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔██╗ ██║██╔═══██╗██║   ██║██╔══██╗   "
echo -e "  ███████║██████╔╝██║     ███████║██║ ██╗██║██║   ██║██║   ██║███████║   "
echo -e "  ██╔══██║██╔══██╗██║     ██╔══██║██║  ████║██║   ██║╚██╗ ██╔╝██╔══██║   "
echo -e "  ██║  ██║██║  ██║╚██████╗██║  ██║██║    ██║╚██████╔╝ ╚████╔╝ ██║  ██║   "
echo -e "  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝    ╚═╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝   "
echo -e "-------------------------------------------------------------------------"
mkdir /mnt

echo -e "\nInstalling prereqs...\n$HR"
pacman -S --noconfirm gptfdisk

DISK=/dev/sda
echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK"
read -p "are you sure you want to continue? (y/N)" formatdisk
case $formatdisk in

y|Y|yes|Yes|YES)
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

# disk prep
sgdisk -Z ${DISK} # zap all on disk
#dd if=/dev/zero of=${DISK} bs=1M count=200 conv=fdatasync status=progress
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

# create partitions
sgdisk -n 1:0:+550M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:0 ${DISK}   # partition 2 (Root), default start, remaining

# set partition types
sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}

# label partitions
sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"ROOT" ${DISK}

# make filesystems
echo -e "\nCreating Filesystems...\n$HR"

mkfs.fat -F32 -n "UEFISYS" "${DISK}1"
mkfs.ext4 -L "ROOT" "${DISK}2"
mount -t ext4 "${DISK}2" /mnt
;;
*)
echo "INSTALLATION CANCELLED"
echo "Rebooting in 3 Seconds ..." && sleep 1
echo "Rebooting in 2 Seconds ..." && sleep 1
echo "Rebooting in 1 Second ..." && sleep 1
reboot now
;;
esac

# mount target
mkdir /mnt/boot
mkdir /mnt/boot/efi
mount -t vfat -L UEFISYS /mnt/boot/

echo "--------------------------------------"
echo "-- Arch Install on Main Drive       --"
echo "--------------------------------------"
pacstrap /mnt base base-devel linux linux-firmware vim sudo wget git --noconfirm --needed
genfstab -U /mnt >> /mnt/etc/fstab

cp -r ~/ArchNova /mnt/root/
