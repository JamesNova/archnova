#!/usr/bin/env bash

#-------------------------------------------------------------------------
#   █████╗ ██████╗  ██████╗██╗  ██╗████╗  ██╗ ██████╗ ██╗   ██╗ █████╗ 
#  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔██╗ ██║██╔═══██╗██║   ██║██╔══██╗
#  ███████║██████╔╝██║     ███████║██║ ██╗██║██║   ██║██║   ██║███████║
#  ██╔══██║██╔══██╗██║     ██╔══██║██║  ████║██║   ██║╚██╗ ██╔╝██╔══██║
#  ██║  ██║██║  ██║╚██████╗██║  ██║██║    ██║╚██████╔╝ ╚████╔╝ ██║  ██║
#  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝    ╚═╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝
#-------------------------------------------------------------------------

echo "-------------------------------------------------"
echo "------------- Setting up everything -------------"
echo "-------------------------------------------------"
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib terminus-font
setfont ter-v22b
sed -i 's/^#Para/Para/' /etc/pacman.conf
echo -e "-------------------------------------------------------------------------"
echo -e "   █████╗ ██████╗  ██████╗██╗  ██╗████╗  ██╗ ██████╗ ██╗   ██╗ █████╗    "
echo -e "  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔██╗ ██║██╔═══██╗██║   ██║██╔══██╗   "
echo -e "  ███████║██████╔╝██║     ███████║██║ ██╗██║██║   ██║██║   ██║███████║   "
echo -e "  ██╔══██║██╔══██╗██║     ██╔══██║██║  ████║██║   ██║╚██╗ ██╔╝██╔══██║   "
echo -e "  ██║  ██║██║  ██║╚██████╗██║  ██║██║    ██║╚██████╔╝ ╚████╔╝ ██║  ██║   "
echo -e "  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝    ╚═╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝   "
echo -e "-------------------------------------------------------------------------"
mkdir /mnt

echo -e "\nInstalling prereqs...\n$HR"
pacman -S --noconfirm gptfdisk

echo "-------------------------------------------------"
echo "---------- Select your disk to format -----------"
echo "-------------------------------------------------"
lsblk
echo "Please enter disk to work on: (example /dev/sda)"
read DISK
echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK"
read -p "are you sure you want to continue? (y/N)" formatdisk
case $formatdisk in

y|Y|yes|Yes|YES)
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

# disk prep
sgdisk -Z ${DISK} # zap all on disk
#dd if=/dev/zero of=${DISK} bs=1M count=200 conv=fdatasync status=progress
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

# create partitions
sgdisk -n 1:0:+550M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:+4G ${DISK} # partition 2 (Swap), default start, 4Gb
sgdisk -n 3:0:+35G ${DISK} # partition 3 (Root), default start, 35Gb
sgdisk -n 4:0:0 ${DISK}   # partition 4 (Home), default start, remaining

# set partition types
sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8200 ${DISK}
sgdisk -t 3:8300 ${DISK}
sgdisk -t 4:8302 ${DISK}

# label partitions
sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"SWAP" ${DISK}
sgdisk -c 3:"ROOT" ${DISK}
sgdisk -c 4:"HOME" ${DISK}

# make filesystems
echo -e "\nCreating Filesystems...\n$HR"
if [[ ${DISK} =~ "nvme" ]]; then
mkfs.fat -F32 -n "UEFISYS" "${DISK}p1"
mkswap -L "SWAP" "${DISK}p2"
swapon "${DISK}2"
mkfs.ext4 -L "ROOT" "${DISK}p3" -f
mkfs.ext4 -L "HOME" "${DISK}p4" -f
mount -t ext4 "${DISK}p3" /mnt
else
mkfs.fat -F32 -n "UEFISYS" "${DISK}1"
mkswap -L "SWAP" "${DISK}2"
swapon "${DISK}2"
mkfs.ext4 -L "ROOT" "${DISK}3"
mkfs.ext4 -L "HOME" "${DISK}4"
mount -t ext4 "${DISK}3" /mnt
fi
;;
*)
echo "INSTALLATION CANCELLED"
echo "Rebooting in 3 Seconds ..." && sleep 1
echo "Rebooting in 2 Seconds ..." && sleep 1
echo "Rebooting in 1 Second ..." && sleep 1
reboot now
;;
esac

# mount target
mkdir /mnt/boot
mkdir /mnt/boot/efi
mkdir /mnt/home
mount -t vfat -L UEFISYS /mnt/boot/
mount -t ext4 -L HOME /mnt/home/

echo "--------------------------------------"
echo "-- Arch Install on Main Drive       --"
echo "--------------------------------------"
pacstrap /mnt base base-devel linux linux-firmware vim sudo wget git --noconfirm --needed
genfstab -U /mnt >> /mnt/etc/fstab

cp -r ~/ArchNova /mnt/root/
