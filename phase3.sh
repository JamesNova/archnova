#!/usr/bin/env bash

echo "--------------------------------------"
echo "--          Network Setup           --"
echo "--------------------------------------"
pacman -S networkmanager wpa_supplicant dhclient --noconfirm --needed
systemctl enable NetworkManager
systemctl enable wpa_supplicant

echo -e "\nInstalling Bootloader\n"

pacman -S grub efibootmgr dosfstools os-prober mtools net-tools --noconfirm --needed

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
echo 'GRUB_DISABLE_OS_PROBER="false"' >> /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
