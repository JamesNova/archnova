#!/usr/bin/env bash

echo -e "-------------------------------------------------------------------------"
echo -e "   █████╗ ██████╗  ██████╗██╗  ██╗████╗  ██╗ ██████╗ ██╗   ██╗ █████╗    "
echo -e "  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔██╗ ██║██╔═══██╗██║   ██║██╔══██╗   "
echo -e "  ███████║██████╔╝██║     ███████║██║ ██╗██║██║   ██║██║   ██║███████║   "
echo -e "  ██╔══██║██╔══██╗██║     ██╔══██║██║  ████║██║   ██║╚██╗ ██╔╝██╔══██║   "
echo -e "  ██║  ██║██║  ██║╚██████╗██║  ██║██║    ██║╚██████╔╝ ╚████╔╝ ██║  ██║   "
echo -e "  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝    ╚═╝ ╚═════╝   ╚═══╝  ╚═╝  ╚═╝   "
echo -e "-------------------------------------------------------------------------"
echo ""

read -p "Are you using this script for a virtual machine? (y/N)" vbox
case $vbox in

y|Y|yes|Yes|YES)
echo "-----------------------------------------"
echo "-- Running Installation for VirtualBox --"
echo "-----------------------------------------"
bash vbox.sh
arch-chroot /mnt /root/ArchNova/phase2.sh
arch-chroot /mnt /root/ArchNova/phase3.sh
echo ""
echo "FINISHED!"
echo ""
echo "Now shutdown and then remove the iso form virtualbox." && sleep 2
umount -R /mnt
echo "Shutting down in 3 seconds ..." && sleep 1
echo "Shutting down in 2 seconds ..." && sleep 1
echo "Shutting down in 1 seconds ..." && sleep 1
shutdown now
;;
*)
echo "-----------------------------------------"
echo "------ Running Installation for PC ------"
echo "-----------------------------------------"
bash phase1.sh
arch-chroot /mnt /root/ArchNova/phase2.sh
arch-chroot /mnt /root/ArchNova/phase3.sh
umount -R /mnt
echo ""
echo "FINISHED!"
echo ""
echo "Rebooting in 5 seconds ..." && sleep 1
echo "Rebooting in 4 seconds ..." && sleep 1
echo "Rebooting in 3 Seconds ..." && sleep 1
echo "Rebooting in 2 Seconds ..." && sleep 1
echo "Rebooting in 1 Second ..." && sleep 1
reboot
;;
esac
